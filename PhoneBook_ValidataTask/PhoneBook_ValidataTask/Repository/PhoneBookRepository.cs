﻿using PhoneBook_ValidataTask.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
 
namespace PhoneBook_ValidataTask.Repository
{
    public class PhoneBookRepository
    {
        public List<PhoneBook> phoneBookList;

        public void Add(PhoneBook phoneBook)
        {
            phoneBookList.Add(phoneBook);
        }

        public void Delete(PhoneBook phoneBook)
        {
            PhoneBook deleted = phoneBookList.FirstOrDefault(x => x.FirstName == phoneBook.FirstName && x.LastName == phoneBook.LastName &&
                                                                x.Type == phoneBook.Type && x.Number == phoneBook.Number);

            phoneBookList.Remove(deleted);
        }

        public void Edit(PhoneBook oldPhoneBook,PhoneBook newPhoneBook)
        {
            int index=phoneBookList.FindIndex(x => x.FirstName == oldPhoneBook.FirstName && x.LastName == oldPhoneBook.LastName &&
                                                                x.Type == oldPhoneBook.Type && x.Number == oldPhoneBook.Number);
            if (index!=-1)
                phoneBookList[index] = newPhoneBook;
        }

        public PhoneBookRepository(List<PhoneBook> _phoneBookList)
        {
            phoneBookList = _phoneBookList;
        }

        public List<PhoneBook> getAll()
        {
            return phoneBookList.OrderBy(x=>x.FirstName).ThenBy(x=>x.LastName).ToList();// the list is ordered in alphabetical order
        }
    }
}
