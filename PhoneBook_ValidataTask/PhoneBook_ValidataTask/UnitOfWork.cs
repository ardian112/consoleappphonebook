﻿using PhoneBook_ValidataTask.Repository;
using PhoneBook_ValidataTask.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace PhoneBook_ValidataTask
{
    public class UnitOfWork
    {
        private BinaryFormatter formatter;
        private string fileName;
        private PhoneBookRepository repository;


        public UnitOfWork(string _fileName, PhoneBookRepository _repository)
        {
            this.formatter = new BinaryFormatter();
            this.fileName = _fileName;
            this.repository = _repository;
        }

        public void Save()
        {
            try
            {
                // Create a FileStream to write data to file.
                FileStream writerFileStream = new FileStream(fileName, FileMode.Create, FileAccess.Write);
                // Save the data to file
                this.formatter.Serialize(writerFileStream, this.repository.phoneBookList);

                // Close the writerFileStream 
                writerFileStream.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Unable to save the data\n" +
                                  "ErroMessage=" + e.Message);
            } 
        }


        public void Load()
        {
            if (File.Exists(this.fileName))
            {

                try
                {
                    // Create a FileStream to read the data
                    FileStream readerFileStream = new FileStream(this.fileName,FileMode.Open, FileAccess.Read);

                    // get the data from file
                    this.repository.phoneBookList = (List<PhoneBook>) this.formatter.Deserialize(readerFileStream);
                    readerFileStream.Close();

                }
                catch (Exception e)
                {
                    Console.WriteLine("Unable to read the data\n" +
                                    "ErroMessage="+e.Message);
                } 

            } 

        } 
    }
}
