﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneBook_ValidataTask.Models
{ 
    //It is needed to mark the class as "Serializable" to save its data to the file
    [Serializable]
    public class PhoneBook
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public PhoneTypeEnum Type { get; set; }

        public string Number { get; set; }


        public PhoneBook()
        {

        }
        public PhoneBook(string firstName, string lastName,PhoneTypeEnum type,string number)
        {
            FirstName = firstName;
            LastName = lastName;
            Type = type;
            Number = number;
        }
    }
}
