﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneBook_ValidataTask.Models 
{
    //create enum to hold PhoneType information in type-save
    public enum PhoneTypeEnum
    {
        Work, Cellphone, Home
    }
}
