﻿
using PhoneBook_ValidataTask.Repository;
using PhoneBook_ValidataTask.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneBook_ValidataTask
{
   
    class Program
    {
        static void Main(string[] args)
        {
            string filename = AppDomain.CurrentDomain.BaseDirectory+"\\PhoneBookList"; //set the file name in the same directory with the application

            List<PhoneBook> phoneBookList = new List<PhoneBook>();
            PhoneBookRepository repository = new PhoneBookRepository(phoneBookList);
            UnitOfWork db = new UnitOfWork(filename, repository);


            /*Use Cases*/

            // 1. Load the data from file
            db.Load();

            // 2. Add the Data
            PhoneBook phoneBook = new PhoneBook("Ardian", "Derstila", PhoneTypeEnum.Home, "221");
            repository.Add(phoneBook);
            repository.Add(new PhoneBook("Ardian", "Derstila", PhoneTypeEnum.Home, "221")); //add "aIljas" to test the order :p :)

           //repository.Delete(phoneBook);
            //repository.Edit(phoneBook);

            //PhoneBook phoneBook1 = new PhoneBook("Ardian", "Derstila", PhoneTypeEnum.Home, "111");
            //repository.Delete(phoneBook);
            //repository.Delete(new PhoneBook("Riemro", "aDerstila", PhoneTypeEnum.Home, "111")); //add "aIljas" to test the order :p :)



            // 3. Edit the data
            PhoneBook phoneBook123 = new PhoneBook("Ardian123", "Ardian123", PhoneTypeEnum.Cellphone, "123");
            repository.Edit(phoneBook, phoneBook123); //phoneBook=old entry, phoneBook123=new entry

            // 4. Remove the data
            repository.Delete(phoneBook123);
            
            db.Save(); //save the object(phoneBookList) to binanry file


            foreach (var item in repository.getAll())
            {
                Console.WriteLine(string.Format("FirstName={0}, LastName={1}, Type={2}, Number={3}",
                                                item.FirstName, item.LastName, item.Type, item.Number));
            }

            Console.ReadLine();
        }
    }
}
